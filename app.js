'use strict';

const express = require('express');
const html = require('./html.js');
const httpProbe = require('./probes/http.js');
const sslProbe = require('./probes/ssl.js');

const app = module.exports = express();

const probes = [
    new httpProbe('http://ohnopub.net/'),
    new httpProbe('http://sam.ohnopub.net/'),
    new httpProbe('http://slatepermutate.org/'),
    new httpProbe('http://hurin.ohnopub.net/'),
    new httpProbe('http://calvin.edu/'),
    new httpProbe('http://direct-connex.com/'),
    new sslProbe('slatepermutate.org', 443),
    new sslProbe('js.ohnopub.net', 443),
    new sslProbe('irc-hub.ohnopub.net', 6697),
];

const probeList = function (probes) {
    const results = [];
    for (let probe of probes) {
	results.push(html`<li>${probe.render()}</li>`);
    }
    return html`<ul>${html.raw(results.join('\n'))}</ul>`;
};

app.get('/', function (request, response) {
    response.set('Content-Type', 'application/xhtml+xml; charset=utf-8');
    response.send(`${html.page(html`<title>Oh! No! Pub Status</title>
<meta name="viewport" content="initial-scale=1"/>`, html`<h1>Oh! No! Pub Status</h1>
${probeList(probes)}`)}`);
});
