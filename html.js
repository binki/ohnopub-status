'use strict';

const RawHtml = function (html) {
    if (typeof html !== 'string') {
	throw new TypeError('RawHtml: Expected string.');
    }
    this.html = html;
};
RawHtml.prototype.toHtml = RawHtml.prototype.toString = function () {
    return this.html;
};

const html = module.exports = function (parts) {
    const args = Array.prototype.slice.call(arguments, 1);
    const result = [];
    parts = parts.slice();
    while (true) {
	result.push(parts.shift());
	if (!parts.length) {
	    break;
	}
	let arg = args.shift();
	if ((arg || {}).toHtml) {
	    result.push(arg.toHtml());
	} else {
	    result.push(
		`${arg}`
		    .replace(/&/g, '&amp;')
		    .replace(/</g, '&lt;')
		    .replace(/>/g, '&gt;'));
	}
    }
    return new RawHtml(result.join(''));
};

html.RawHtml = RawHtml;
html.html = html;
html.raw = html => new RawHtml(html);

html.page = function (head, body) {
    return html`<?xml version="1.0"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    ${head}
  </head>
  <body>
    ${body}
  </body>
</html>`;
};
