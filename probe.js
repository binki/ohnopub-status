'use strict';

const delay = require('delay');
const events = require('events');
const html = require('./html.js');

class Probe extends events.EventEmitter {
    constructor(name) {
	super();

	this.lastSeen = null;
	this.name = name;
	this.status = {
	    type: 'unknown',
	};

	this.on('set', () => {
	    if (this.status.type === 'alive') {
		this.lastSeen = new Date();
	    }
	});
	console.log(`name: ${name}`);
    }

    destroy() {
	this.emit('destroy');
    }

    set(status) {
	this.status = Object.assign({}, {
	    type: 'alive',
	}, status);
	this.emit('set');
    }

    setError(ex) {
	this.set({
	    type: 'error',
	    ex: ex,
	});
    }

    render() {
	return html`<code>${this.name}</code>: <span class="status-${this.status.type}">${this.status.type}</span>${this.status.type === 'error' ? ` ${this.status.ex}` : ''}${this.lastSeen ? ` (seen: ${this.lastSeen})` : ''}`;
    }
};

const interval = 30000;
class PollingProbe extends Probe {
    constructor(name) {
	super(name);

	this.destroyed = false;

	this.on('destroy', () => this.destroyed = true);

	this.check();
    }

    check() {
	if (this.destroyed) {
	    return;
	}

	Promise.resolve()
	    .then(() => this._check())
	    .then(status => this.set(status))
	    .catch(ex => this.setError(ex))
	    .then(delay(interval))
	    .then(() => this.check())
	;
    }
};

module.exports.Probe = Probe;
module.exports.PollingProbe = PollingProbe;
