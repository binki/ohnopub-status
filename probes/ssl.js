'use strict';

const html = require('../html.js');
const PollingProbe = require('../probe.js').PollingProbe;
const tls = require('tls');

class SslProbe extends PollingProbe {
    constructor(host, port) {
	super(`${host}:${port}`);

	this.certDate = null;
	this.host = host;
	this.port = port;
    }

    _check() {
	return Promise.resolve()
	    .then(() => new Promise((resolve, reject) => {
		const tlsSocket = tls.connect(this.port, this.host, {
		    servername: this.host,
		}, () => {
		    resolve({
			certDate: (tlsSocket.getPeerCertificate() || {}).valid_to,
		    });
		    tlsSocket.end();
		}).on('data', () => {}).on('error', reject);
	    }));
    }

    render() {
	return html`${super.render()}${this.status.certDate ? html` (expires: ${this.status.certDate})` : ''}`;
    }
};

module.exports = SslProbe;
