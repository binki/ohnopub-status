'use strict';

const html = require('../html.js');
const http = require('http');
const PollingProbe = require('../probe.js').PollingProbe;

/* promisify is broken with http, so we can’t use it. */
/*
const promisify = require('promisify-node');
const http = promisify('http');
 */

class HttpProbe extends PollingProbe {
    constructor(uri) {
	super(uri);

	this.uri = uri;
    }

    _check() {
	return Promise.resolve()
	    .then(() => new Promise((resolve, reject) => {
		http.get(this.uri, resolve).on('error', reject);
	    }))
	    .then(response => ({
		statusCode: response.statusCode,
	    }))
	;
    }

    render() {
	return html`${super.render()}${this.status.statusCode ? ` code=${this.status.statusCode}`: ''}`;
    }
};

module.exports = HttpProbe;
